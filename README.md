# BACKEND-CHALLENGE
> Backend CHALLENGE PROJECT.

## How to use

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:port
$ npm run dev

# build for production and launch server
$ npm run build

$ npm start
```

## How to migrate database schema
```bash
# migrate up
$ npm run db:migrate:up

# migrate down
$ npm run db:migrate:down
```

## How to seed demo data
```bash
# seeder up
$ npm run db:seed:up

# seeder down
$ npm run db:seed:down
```
